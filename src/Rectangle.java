
public class Rectangle implements Shape {
	int a,b;
	
	Rectangle (int a, int b) {
		this.a=a;
		this.b=b;
	}
	@Override
	public double getPerimeter() {
		// TODO Auto-generated method stub
		return 2*(a+b);
	}

	@Override
	public double getSquare() {
		// TODO Auto-generated method stub
		return a*b;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Rectangle";
	}
	@Override
	public int getAngles() {
		// TODO Auto-generated method stub
		return 4;
	}
	
}
