
public class parallelogram implements Shape{
	int a,b;
	double f;
	parallelogram (int a, int b, float f) {
		this.a=a;
		this.b=b;
		this.f=f;
	}
	@Override
	public double getPerimeter() {
		// TODO Auto-generated method stub
		return 2*(a+b);
	}

	@Override
	public double getSquare() {
		// TODO Auto-generated method stub
		return a*b;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Parallelogram";
	}
	@Override
	public int getAngles() {
		// TODO Auto-generated method stub
		return 4;
	}

}
