public interface Shape extends LineGroup{
	public double getSquare();
	public String getName();
	public int getAngles();
}