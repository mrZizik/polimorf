
public class Circle implements Shape {
	int r;
	Circle (int a) {
		this.r=a;
	}
	@Override
	public double getPerimeter() {
		// TODO Auto-generated method stub
		return 2*3.14*r;
	}

	@Override
	public double getSquare() {
		// TODO Auto-generated method stub
		return 3.14*Math.pow(r, 2);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Circle";
	}
	@Override
	public int getAngles() {
		// TODO Auto-generated method stub
		return 0;
	}

}
